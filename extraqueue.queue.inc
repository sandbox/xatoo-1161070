<?php

/**
 * @file
 * Enriched queue functionality.
 */

define('EXTRAQUEUE_UNLIMITED', -1);

interface ExtraQueueInterface extends DrupalQueueInterface {

  /**
   * Remove all items from the queue.
   *
   * This does in fact the same as deleteQueue but allows some queue implementations
   * to remove all items from a queue while keeping an empty container.
   */
  public function clear();

  /**
   * Count the number of occurences of an item.
   *
   * This function might be slow on queues which do not index item data.
   *
   * @param $data
   *   The data to search for in the queue.
   * @return
   *   An integer estimate of the number of items in the queue which equal $data.
   */
  public function count($data);

  /**
   * Returns an the next unclaimed item object without claiming it.
   *
   * This function does not lock an item and is therefore not suited to use in item
   * processing. Only use this function if you don't care that the item gets claimed
   * by another process.
   *
   * @return
   *   On success we return an item object. If there where no unclaimed items in the
   *   queue it returns false.
   */
  public function peekItem();

  /**
   * Returns an array with unclaimed items in the queue.
   *
   * This function does not lock items and is therefore not suited to use in item
   * processing. Only use this function if you don't care whether the items get
   * claimed by another process.
   *
   * @param $upperbound
   *   An integer value which denotes the maximum number of items to return.
   *
   * @return
   *   On success we return a array with item objects. If there where no unclaimed
   *   items in the queue it returns an empty array.
   */
  public function peekItems($upperbound = EXTRAQUEUE_UNLIMITED);

  /**
   * Retrieve the number of unclaimed items in the queue.
   *
   * Note that during heavy queue using this number will be a best guess of the
   * current state.
   *
   * @return
   *   An integer estimate of the number of unclaimed items in the queue.
   */
  public function numberOfUnclaimedItems();

  /**
   * Retrieve the number of claimed items in the queue.
   *
   * Note that during heavy queue using this number will be a best guess of the
   * current state.
   *
   * @return
   *   An integer estimate of the number of claimed items in the queue.
   */
  public function numberOfClaimedItems();

  /**
   * Force all items in the queue to be released.
   *
   * @return
   *   An integer value representing the number of items which were locked before
   *   the execution.
   */
  public function releaseAll();
}

/**
 * ExtraQueue implementation.
 */
class ExtraQueue extends SystemQueue implements ExtraQueueInterface {

  /**
   * In the SystemQueue implementation, deleting a queue
   * equals deleting all items in a queue.
   */
  public function clear() {
    $this->deleteQueue();
  }

  public function count($data) {
    return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND data = :data',
                    array(':name' => $this->name, ':data' => serialize($data)))->fetchField();
  }

  public function peekItem() {
    $item = db_query_range('SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name ORDER BY created ASC', 0, 1, array(':name' => $this->name))->fetchObject();
    if ($item) {
      // If there is a result, then there was un unclaimed item.
      $item->data = unserialize($item->data);
      return $item;
    }
    else {
      // No unclaimed items in the queue.
      return FALSE;
    }
  }

  public function peekItems($upperbound = EXTRAQUEUE_UNLIMITED) {
    $query_string = 'SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name ORDER BY created ASC';
    $results = array();
    if ($upperbound == EXTRAQUEUE_UNLIMITED) {
      $query = db_query($query_string, array(':name' => $this->name));
    }
    else {
      $query = db_query_range($query_string, 0, $upperbound, array(':name' => $this->name));
    }
    while ($item = $query->fetchObject()) {
      $item->data = unserialize($item->data);
      $results[] = $item;
    }
    return $results;
  }

  public function numberOfUnclaimedItems() {
    return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND expire = 0',
                    array(':name' => $this->name))->fetchField();
  }

  public function numberOfClaimedItems() {
    return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND expire != 0',
                    array(':name' => $this->name))->fetchField();
  }

  public function releaseAll() {
    return db_update('queue')
      ->fields(array(
        'expire' => 0
      ))
      ->condition('name', $this->name)
      ->condition('expire', 0, '!=')
      ->execute();
  }
}
